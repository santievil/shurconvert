function salvar() {
	scO = new Object();
	scO.CCapiKey = document.getElementById('key').value;
	scO.CCoutF = document.getElementById('outputF')[document.getElementById('outputF').selectedIndex].value;

	chrome.storage.sync.set({'SCO': scO}, function() {
	var status = document.getElementById('status');
	status.textContent = 'Opciones guardadas';
	setTimeout(function() {
	  status.textContent = '';
	}, 750);
	});
}

function cargar() {
	chrome.storage.sync.get('SCO', function(items) {
		if (items.SCO) {
			document.getElementById('key').value = items.SCO.CCapiKey;
			document.getElementById('outputF').value = items.SCO.CCoutF;
		}
	});	
}

document.addEventListener('DOMContentLoaded', cargar);
document.getElementById('save').addEventListener('click',salvar);