# Shurconvert

Extension para Chrome que convierte videos con la API de CloudConvert

## Para cargarla:

* Los archivos deben de estar en una carpeta (c:\Shurconvert x.ej.)
* En chrome, cargar chrome://extensions/ y cargarla descomprimida
* En la configuración, introducir la API personal de <a href="https://cloudconvert.com/dashboard/api">Cloudconvert</a> y el 
  formato al que queremos convertir
* Cloudconvert tiene una cuenta gratuita que permite convertir hasta 25 
  minutos al día.

## Para utilizarla:

* Click derecho sobre el video o el enlace.
* En el menú contextual aparecerá la acción "Conversor" con su icono
* Pulsa la acción y esperar