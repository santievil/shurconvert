//Identificador de notificaciones
var notIDN = 0;

//Si se pulsa en el botonm te lleva a la ventana de condiguracion
chrome.notifications.onButtonClicked.addListener(
	function buttonCallback (notID, btnIndex) {
		if (notID == "errCFG") {
			chrome.runtime.openOptionsPage();
		}
	}
);

// Create a notification
function muestraNoti (popID, percent, estado, texto, fncallback){
	var titulo = '';
	var accion = '';
	var tipo = '';
	var progreso = 0;

	//Segun el estado, diferentes configuraciones
	switch (estado){
		case 1:
			titulo = 'Terminado';
			notID = popID;
			tipo = 'basic';
			break;
		case 2:
			titulo = 'Procesando';
			notID = 'id' + notIDN++;
			tipo = 'progress';
			break;
		case 3:
			titulo = '¡Ojo cuidao!';
			accion = 'Ir a configuración';
			notID = 'errCFG';
			tipo = 'basic';
			break;
		case 4:
			titulo = 'Procesando';
			notID = popID;
			tipo = 'progress';
			progreso = percent;
			break;
		default:
			titulo = '¡¡¡Error!!!';
			notID = 'err';
			tipo = 'basic';
	}

	//Opciones basicas
	var options = {
		type: tipo,
		title: titulo,
		message: texto,
		iconUrl: 'refresh.png'		
	}

	//Casos particulares
	if (estado == 2 || estado == 3){
		if (estado == 3){
			//Si esl estado es 3, necesita un acceso a la configuracion
			options.buttons= [
					{ title: accion, iconUrl: "refresh.png" },
				];
		}
		chrome.notifications.create(notID, options, function (notID){
			if (fncallback && typeof(fncallback) === "function"){fncallback();}
			if (estado == 3){
				setTimeout(function() {
					chrome.notifications.clear(notID);
				}, 5000);
			}
		});
	}else{
		if (estado == 4){
			options.progress = progreso;
		}
		chrome.notifications.update(notID, options, function (notID){
			if (fncallback && typeof(fncallback) === "function"){fncallback();}
			if (estado == 0 || estado == 1){
				setTimeout(function() {
					chrome.notifications.clear(notID);
				}, 5000);
			}
		});
	}
	
	//Devolvemos el id de la notificacion
	return notID;
	
}

var CCapiKey = '';
var CCoutF = '';

//Se define el solo una vez el conversor
chrome.runtime.onInstalled.addListener(function(){
	var menuItem = {
		"id": "conversor",
		"title": "Conversor",
		"contexts": ["link", "video"]
	};	
	chrome.contextMenus.create(menuItem);
});

//Cada vez que le pinchemos
chrome.contextMenus.onClicked.addListener(function(clickData){
	chrome.storage.sync.get('SCO', function(items) {
		if (items.SCO) {
			CCapiKey = items.SCO.CCapiKey;
			CCoutF = items.SCO.CCoutF;
			
			if (CCapiKey == '' && CCoutF == ''){
				var msg="Por favor, configura la extensión";
				muestraNoti(null,null,3,msg,null);
			}else{
				//Primera peticion: Para obtener el id del proceso
				$.ajax({
					type: "POST",
					url: 'https://api.cloudconvert.com/process',
					data: {
						'apikey': CCapiKey,
						'inputformat': clickData.srcUrl.substring(clickData.srcUrl.lastIndexOf(".")+1),
						'outputformat': CCoutF
					},
					success: function(data)
					{
						var nombre = clickData.srcUrl.substring(clickData.srcUrl.lastIndexOf("/")+1,clickData.srcUrl.lastIndexOf(".")-1);
						var msg = 'Video procesandose: '+ nombre;
						var popID = muestraNoti(null,null,2,msg, function(){
							//Segunda peticion: Convertimos el video
							$.ajax({
								type: "POST",
								url: 'https:'+data.url,
								data: {
									'wait': false, //true
									'input': 'download',
									'file': clickData.srcUrl,
									'outputformat': CCoutF
								},
								success: function(data)
								{	
									/*Aqui repetimos la llamada las veces que haga falta.
									Nos dice el % que lleva de conversion
									*/
									var completado = false;
									(function estado() {
									  $.ajax({
										type: "GET",
										url: 'https:'+data.url,										
										success: function(data){											
											// Cuando este al 100% se descarga, si no, vuelve a preguntar por el estado
											if (data.percent != 100){
												var msg = 'Video procesandose: '+ nombre;
												muestraNoti(popID,Math.round(data.percent),4,msg,null);
											}else{
												completado = true;
												var msg = 'Coversion realizada correctamente: '+ data.input.name;
												muestraNoti(popID,null,1,msg, function(){
													var link = document.createElement("a");
													link.download = data.input.name;
													link.href = 'https:'+data.output.url;
													link.click();
												});
											}
										},
										complete: function() {
											if (completado == false){
												setTimeout(estado, 1000);
											}
										}
									  });
									})();
								},
								//Si falla la conversion
								error: function(data){
									var msg = data.responseJSON.error;
									muestraNoti(null,null,0,msg,null);
								}
							});
						});
					},
					//Si falla la solicitu del proceso
					error: function(data){
						var msg = data.responseJSON.error;
						muestraNoti(null,null,0,msg,null);
					}
				});
			}
		}else{
			//Si no tenemos configurada la extension
			var msg="Por favor, configura la extensión";
			muestraNoti(null,null,3,msg,null);
		}
	});
});
